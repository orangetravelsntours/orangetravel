import colors from 'vuetify/es5/util/colors'

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {

    meta: [
       
      { charset: 'utf-8' },
     { name: 'theme-color' , content: '#000000' },
     { name: 'msapplication-navbutton-color' , content: '#000000' },
     { name: 'apple-mobile-web-app-capable' , content: 'yes' },
     { name: 'apple-mobile-web-app-status-bar-style'  , content: '#000000' },

     { name: 'viewport', content: 'width=device-width, initial-scale=1' },
     { hid: 'description', name: 'description', content: '' },
     { hid: 'description', name: 'keywords', content: '' },

     { property:'og:locale' , content:'af-ZA' },
     { property:'og:type' ,content:'website' },
     { property:'og:title' ,content:'WineGarage Uganda Kampala' },
     { property: 'og:description' ,content:'buy digital African Art with bitcoin,African landscape,African gifs,and rootsman pics' },
     { property:'og:site_name' ,content:'winegarage' },
     { property:'og:url' ,content: ''},
     {name:'twitter:card' ,content: 'summary'},
     {name:'twitter:description', content:'Discover the magic of African Music at Mansaah, a community powered entertainment Rootsman vibe. Lift your spirits with hard beats mixtapes and audios, trending music, create playlists, inspiring podcasts, viral  African albums, and so much more!!!!'},
     { name:'twitter:creator' ,content: '@kachaq'},
     {name: 'twitter:site', content: '@kachaq'},
     { name:'twitter:title', content:''},
     { name:'twitter:image', content:''},
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    {rel:'stylesheet', href:'https://fonts.googleapis.com/css?family=Caveat|Domine|Hind|Indie+Flower|Quicksand|Roboto&display=swap'}
     

    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: "#3b8070",throttle: 0  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/main.css',
    '~/assets/styles.css',
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: "~/plugins/vue-masonry-css", ssr: false },
    { src: '~plugins/SocialSharing.js', mode: 'client' },
    { src: '~plugins/aos.js', ssr: false}
    
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
      '@nuxtjs/pwa',
    '@nuxtjs/axios'
  ],
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    }
  }
}
