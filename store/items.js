function getRandomCategory() {
  const categoryArr = ['Wine', 'Spirit', 'Whiskey']
  return categoryArr[Math.floor(Math.random() * categoryArr.length)]
}

export const state = () => ({
  items: [
    {
      id:'1',
      Product_tag:'Savage Winery',
      Product_caption:'Whiskey',
      Product_price:10,
      Product_image:'http://source.unsplash.com/6laahjSsGws'

    },
    {
      id:'2',
      Product_tag:'Red Red Wine',
      Product_caption:'Uganda',
      Product_price:20,
      Product_image:'http://source.unsplash.com/TXi0fsYFlu0'
    },
    {
      id:'3',
      Product_tag:'WineGarage',
      Product_caption:'Terracura Personalised Bottle',
      Product_price:22,
      Product_image:'http://source.unsplash.com/4uqZBPTEJaU'
    },
  ]
})

export const mutations = {
  setItems(state, items) {
    state.items = items
  },
  addToCart(state, id) {
    const item = state.items.find((el) => el.id === id)
    item.isReserved = true
  },
  removeFromCart(state, id) {
    const item = state.items.find((el) => el.id === id)
    item.isReserved = false
  }
}


export const getters = {
  items: (state) => state.items
}
